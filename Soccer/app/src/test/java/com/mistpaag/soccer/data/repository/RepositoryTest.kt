package com.mistpaag.soccer.data.repository

import com.mistpaag.soccer.dto.Event
import com.mistpaag.soccer.dto.Team
import com.mistpaag.soccer.dto.TeamDetail
import com.mistpaag.soccer.dto.responses.OperationResult
import com.mistpaag.soccer.utils.ConstantsObject
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import org.junit.Test

import org.junit.Assert.*

class RepositoryTest {

    private val fakeRepository = FakeRepository()
    private val fakeErrorRepository = FakeErrorRepository()

    private val testDispatcher = MainCoroutineRule()
    @Test
    fun fetchTeamsTest() = testDispatcher.runBlocking {
        val result = fakeRepository.fetchTeams(ConstantsObject.ID_SPANISH_LEAGUE).first()
        when (result) {
            is OperationResult.Success -> assertEquals(fakeRepository.mockList, result.data)
        }
    }

    @Test
    fun fetchTeamsErrorTest() = testDispatcher.runBlocking {
        val result = fakeErrorRepository.fetchTeams(ConstantsObject.ID_SPANISH_LEAGUE).first()
        when (result) {
            is OperationResult.Error -> assertEquals(fakeErrorRepository.mockException, result.exception)
        }
    }


}

class FakeErrorRepository:IRepository {
    val mockException = "Ocurrio un error"

    override fun fetchTeams(idLeague: Int): Flow<OperationResult<List<Team>>> {
        return flow { emit(OperationResult.Error(mockException)) }
    }

    override fun fetchTeam(idTeam: String): Flow<OperationResult<TeamDetail>> {
        TODO("Not yet implemented")
    }

    override fun fetchNextEvents(idTeam: String): Flow<OperationResult<List<Event>>> {
        TODO("Not yet implemented")
    }
}

class FakeRepository:IRepository {

    val mockList:MutableList<Team>  = mutableListOf()

    init {
        mockData()
    }

    private fun mockData(){
        mockList.add(Team("0","Barcelona","www.barcelona.com","metropolitano"))
        mockList.add(Team("1","Madrid","www.barcelona.com","metropolitano"))
        mockList.add(Team("2","Alaves","https://www.deportivoalaves.com/","metropolitano"))
    }

    override fun fetchTeams(idLeague: Int): Flow<OperationResult<List<Team>>> {
        return flow { emit(OperationResult.Success(mockList)) }
    }

    override fun fetchTeam(idTeam: String): Flow<OperationResult<TeamDetail>> {
        TODO("Not yet implemented")
    }

    override fun fetchNextEvents(idTeam: String): Flow<OperationResult<List<Event>>> {
        TODO("Not yet implemented")
    }
}
