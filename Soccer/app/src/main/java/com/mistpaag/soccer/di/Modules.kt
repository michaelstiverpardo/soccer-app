package com.mistpaag.soccer.di


import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.mistpaag.soccer.data.remote.ApiService
import com.mistpaag.soccer.data.repository.Repository
import com.mistpaag.soccer.utils.ConstantsObject
import com.mistpaag.soccer.views.detail.DetailViewModel
import com.mistpaag.soccer.views.league.LeagueViewModel
import com.mistpaag.soccer.views.splash.SplashViewModel
import okhttp3.OkHttpClient
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


val mainVMModule = module {
    viewModel { SplashViewModel() }
    viewModel { LeagueViewModel( get() ) }
    viewModel { DetailViewModel( get() ) }
}

val dataModule = module {

    single {
        Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .baseUrl(ConstantsObject.URL_BASE)
            .client(createOkHttpClient())
            .build()
    }

    single { get<Retrofit>().create(ApiService::class.java) }
    single { Repository(get()) }
}

fun createOkHttpClient(): OkHttpClient {
    return OkHttpClient.Builder()
        .connectTimeout(60, TimeUnit.SECONDS)
        .readTimeout(60, TimeUnit.SECONDS)
        .writeTimeout(60, TimeUnit.SECONDS)
        .build()
}
