package com.mistpaag.soccer.dto

data class SocialNetwork (val socialType: String, val url:String)

enum class SocialNetwotkType(val type:String){
    FACEBOOK("facebook"),
    TWITTER("twitter"),
    INSTAGRAM("instagram"),
    WEBSITE("website")
}