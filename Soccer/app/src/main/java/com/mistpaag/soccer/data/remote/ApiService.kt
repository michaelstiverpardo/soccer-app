package com.mistpaag.soccer.data.remote

import com.mistpaag.soccer.dto.Event
import com.mistpaag.soccer.dto.Team
import com.mistpaag.soccer.dto.TeamDetail
import com.mistpaag.soccer.dto.responses.EventResponse
import com.mistpaag.soccer.dto.responses.TeamResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("lookup_all_teams.php")
    fun fetchLeagueTeams(
        @Query("id")id: Int
    ): Deferred<TeamResponse<List<Team>>>

    @GET("lookupteam.php")
    fun fetchTeam(
        @Query("id")id: String
    ): Deferred<TeamResponse<List<TeamDetail>>>

    @GET("eventsnext.php")
    fun fetchNextEvents(
        @Query("id")id: String
    ): Deferred<EventResponse<List<Event>>>
}