package com.mistpaag.soccer.views.detail

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.SyncStateContract
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.MergeAdapter
import com.mistpaag.soccer.R
import com.mistpaag.soccer.databinding.DetailFragmentBinding
import org.koin.android.ext.android.inject

class DetailFragment : Fragment() {

    companion object {
        fun newInstance() = DetailFragment()
    }

    private val viewModel by inject<DetailViewModel> ()
    private lateinit var binding: DetailFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.detail_fragment, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        binding.backImage.setOnClickListener {
            findNavController().popBackStack()
        }

        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let {
            val idTeam = DetailFragmentArgs.fromBundle(it).idTeam
            viewModel.fetchTeam(idTeam)
        }


        val adapter = MergeAdapter()
        val eventAdapter = EventAdapter()

        viewModel.team.observe(viewLifecycleOwner, Observer {
            val descriptionAdapter = DescriptionAdapter(it.getDescription())
            descriptionAdapter.notifyDataSetChanged()

            val socialNetWorkAdapter = SocialNetWorkAdapter(it.getSocialNetworks()){ url->
                gotoUrl(url)
            }
            socialNetWorkAdapter.notifyDataSetChanged()

            adapter.addAdapter(descriptionAdapter)
            adapter.addAdapter(eventAdapter)
            adapter.addAdapter(socialNetWorkAdapter)
        })

        viewModel.nextEvents.observe(viewLifecycleOwner, Observer {
            eventAdapter.submitList(it.toMutableList())
        })

        binding.mergeRecycler.adapter = adapter
    }

    fun gotoUrl(urlStr:String){
        var url = urlStr
        val urlIntent = Intent(Intent.ACTION_VIEW)
        urlIntent.data = Uri.parse(url)
        requireActivity().startActivity(urlIntent)
    }
}