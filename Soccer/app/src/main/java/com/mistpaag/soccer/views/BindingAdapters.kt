package com.mistpaag.soccer.views

import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.mistpaag.soccer.dto.Team
import com.mistpaag.soccer.views.detail.DescriptionAdapter
import com.mistpaag.soccer.views.league.LeagueAdapter

@BindingAdapter("imageUrl")
fun bindImage(imgView: ImageView, imgUrl: String?) {
    imgUrl?.let{url->
        imgView.load(url)
    }

}

@BindingAdapter("listData")
fun bindRecyclerView(recyclerView: RecyclerView, data: List<Team>) {
    val adapter = recyclerView.adapter as LeagueAdapter
    adapter.submitList(data.toMutableList())
}

@BindingAdapter("isVisible")
fun bindProgressBarView(progressBar: ProgressBar, visible:Boolean) {
    progressBar.isVisible = visible
}


