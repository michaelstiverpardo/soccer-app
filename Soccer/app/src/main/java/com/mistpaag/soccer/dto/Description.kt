package com.mistpaag.soccer.dto

data class Description(val description: String, val jerseyUrl:String?) {
}