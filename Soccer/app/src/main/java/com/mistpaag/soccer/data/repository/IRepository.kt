package com.mistpaag.soccer.data.repository

import com.mistpaag.soccer.dto.Event
import com.mistpaag.soccer.dto.Team
import com.mistpaag.soccer.dto.TeamDetail
import com.mistpaag.soccer.dto.responses.OperationResult
import kotlinx.coroutines.flow.Flow

interface IRepository {
    fun fetchTeams(idLeague: Int): Flow<OperationResult<List<Team>>>
    fun fetchTeam(idTeam: String): Flow<OperationResult<TeamDetail>>
    fun fetchNextEvents(idTeam: String): Flow<OperationResult<List<Event>>>
}