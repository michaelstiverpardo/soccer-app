package com.mistpaag.soccer.data.repository

import com.mistpaag.soccer.data.remote.ApiService
import com.mistpaag.soccer.dto.Team
import com.mistpaag.soccer.dto.responses.OperationResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class Repository(private val remoteData: ApiService):IRepository {

    override fun fetchTeam(idTeam: String) = flow{
        try {
            val teams = remoteData.fetchTeam(idTeam).await()
            emit(OperationResult.Success(teams.teams?.get(0)))
        }catch (e:Exception){
            emit(obtainErrorResult(e))
        }
    }.flowOn(Dispatchers.IO)

    override fun fetchNextEvents(idTeam: String) = flow{
        try {
            val data = remoteData.fetchNextEvents(idTeam).await()
            emit(OperationResult.Success(data.events))
        }catch (e:Exception){
            emit(obtainErrorResult(e))
        }
    }.flowOn(Dispatchers.IO)

    private fun obtainErrorResult(e:Exception): OperationResult.Error {
        return OperationResult.Error(e.localizedMessage.toString())
    }

    override fun fetchTeams(idLeague: Int)= flow {
        try {
            val teams = remoteData.fetchLeagueTeams(idLeague).await()
            emit(OperationResult.Success(teams.teams))
        }catch (e:Exception){
            emit(obtainErrorResult(e))
        }
    }.flowOn(Dispatchers.IO)


}