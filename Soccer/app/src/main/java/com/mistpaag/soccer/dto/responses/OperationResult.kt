package com.mistpaag.soccer.dto.responses

import com.mistpaag.soccer.dto.Team
import kotlinx.coroutines.flow.Flow

sealed class OperationResult<out T: Any> {
    data class Success<T: Any>(val data: T?) : OperationResult<T>()
    data class Error(val exception:String) : OperationResult<Nothing>()
}