package com.mistpaag.soccer.views.detail

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.mistpaag.soccer.databinding.DetailSocialNetworkItemBinding
import com.mistpaag.soccer.dto.SocialNetwork
import com.mistpaag.soccer.dto.SocialNetwotkType

class SocialNetWorkAdapter(private val list: List<SocialNetwork>, val itemClick:(String)-> Unit): RecyclerView.Adapter<SocialNetWorkAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = DetailSocialNetworkItemBinding.inflate(LayoutInflater.from(parent.context),parent, false)
        return ViewHolder(view,itemClick)
    }

    override fun getItemCount() = 1

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(list)
    }

    inner class ViewHolder(private var binding: DetailSocialNetworkItemBinding, val itemClick: (String) -> Unit): RecyclerView.ViewHolder(binding.root) {
        lateinit var list : ArrayList<SocialNetwork>
        fun bindView(list: List<SocialNetwork>){
            this.list = list as ArrayList<SocialNetwork>
            with(binding){
                list.forEach { socialNetwork ->
                    when(socialNetwork.socialType){
                        SocialNetwotkType.WEBSITE.type -> {
                            showImage(websiteImage, socialNetwork.url)
                        }
                        SocialNetwotkType.FACEBOOK.type -> {
                            showImage(facebookImage, socialNetwork.url)
                        }
                        SocialNetwotkType.TWITTER.type -> {
                            showImage(twitterImage, socialNetwork.url)
                        }
                        SocialNetwotkType.INSTAGRAM.type -> {
                            showImage(instagramImage, socialNetwork.url)
                        }
                    }
                }

            }
            binding.executePendingBindings()
        }

        fun showImage(imageView: ImageView, url: String) {
            imageView.isVisible = !url.isNullOrEmpty()
            imageView.setOnClickListener { itemClick(url) }
        }


    }



}