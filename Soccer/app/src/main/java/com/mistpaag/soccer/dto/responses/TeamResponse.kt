package com.mistpaag.soccer.dto.responses

data class TeamResponse<T>(val teams: T?=null)
