package com.mistpaag.soccer.dto

import com.google.gson.annotations.SerializedName


data class Event(
    @SerializedName("idEvent")
    val id: String,
    @SerializedName("strHomeTeam")
    val home:String,
    @SerializedName("strAwayTeam")
    val away:String,
    @SerializedName("dateEvent")
    val date: String,
    @SerializedName("strTime")
    val time:String
)