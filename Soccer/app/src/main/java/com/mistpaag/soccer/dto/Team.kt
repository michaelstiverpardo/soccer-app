package com.mistpaag.soccer.dto

import com.google.gson.annotations.SerializedName


data class Team(
    @SerializedName("idTeam")
    val id: String,
    @SerializedName("strTeam")
    val name:String,
    @SerializedName("strTeamBadge")
    val teamBadge: String,
    @SerializedName("strStadium")
    val stadium:String
)