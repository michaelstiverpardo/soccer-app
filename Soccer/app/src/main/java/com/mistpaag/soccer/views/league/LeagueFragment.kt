package com.mistpaag.soccer.views.league

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mistpaag.soccer.R
import com.mistpaag.soccer.databinding.LeagueFragmentBinding
import org.koin.android.ext.android.inject

class LeagueFragment : Fragment() {

    companion object {
        fun newInstance() = LeagueFragment()
    }

    private val viewModel by inject<LeagueViewModel> ()
    private lateinit var binding : LeagueFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.league_fragment, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        viewModel.fetchTeams()

        val adapter = LeagueAdapter{
            goToDetail(it.id)
        }
        binding.leagueRecycler.adapter = adapter


        return binding.root
    }

    fun goToDetail(id:String){
        findNavController().navigate(LeagueFragmentDirections.actionLeagueFragmentToDetailFragment(id))
    }



}