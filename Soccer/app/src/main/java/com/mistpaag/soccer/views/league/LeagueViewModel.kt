package com.mistpaag.soccer.views.league

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mistpaag.soccer.data.repository.Repository
import com.mistpaag.soccer.dto.Team
import com.mistpaag.soccer.dto.TeamDetail
import com.mistpaag.soccer.dto.responses.OperationResult
import com.mistpaag.soccer.utils.ConstantsObject
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class LeagueViewModel(private val repository: Repository) : ViewModel() {
    // TODO: Implement the ViewModel
    val teams : LiveData<List<Team>>
        get()= _teams
    private val _teams = MutableLiveData<List<Team>>(emptyList())

    val loading : LiveData<Boolean>
        get()= _loading
    private val _loading = MutableLiveData<Boolean>(true)

    val tittle : LiveData<String>
        get()= _tittle
    private val _tittle = MutableLiveData<String>("Spanish League")

    fun fetchTeams(){
        viewModelScope.launch {
            repository.fetchTeams(ConstantsObject.ID_SPANISH_LEAGUE).collect {
                when(it){
                    is OperationResult.Success -> {
                        _teams.value = it.data
                        _loading.value = false
                    }
                }
            }
        }
    }

}