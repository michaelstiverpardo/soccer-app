package com.mistpaag.soccer.dto.responses

data class EventResponse<T>(val events: T?=null)
