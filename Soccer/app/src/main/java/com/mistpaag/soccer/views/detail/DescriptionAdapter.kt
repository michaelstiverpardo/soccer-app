package com.mistpaag.soccer.views.detail

import android.view.LayoutInflater

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

import com.mistpaag.soccer.databinding.DetailDescriptionItemBinding
import com.mistpaag.soccer.dto.Description


class DescriptionAdapter(private val description:Description): RecyclerView.Adapter<DescriptionAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = DetailDescriptionItemBinding.inflate(LayoutInflater.from(parent.context),parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = 1

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(description)
    }

    inner class ViewHolder(private var binding: DetailDescriptionItemBinding): RecyclerView.ViewHolder(binding.root) {
        fun bindView(description: Description){
            binding.property = description
            binding.executePendingBindings()
        }

    }



}