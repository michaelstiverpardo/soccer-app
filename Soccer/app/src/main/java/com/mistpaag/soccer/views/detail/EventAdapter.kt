package com.mistpaag.soccer.views.detail

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ListAdapter
import androidx.core.net.toUri
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.mistpaag.soccer.R
import com.mistpaag.soccer.databinding.DetailNextEventItemBinding
import com.mistpaag.soccer.databinding.LeagueItemBinding
import com.mistpaag.soccer.dto.Event
import com.mistpaag.soccer.dto.Team
import kotlin.properties.Delegates

class EventAdapter() : androidx.recyclerview.widget.ListAdapter<Event, EventAdapter.ViewHolder>(EventDiffCallback()){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(DetailNextEventItemBinding.inflate(LayoutInflater.from(parent.context),parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindTo(getItem(position))
    }

    inner class ViewHolder(private var binding: DetailNextEventItemBinding) : RecyclerView.ViewHolder(binding.root){
        fun bindTo(event: Event){
            binding.homeTeam.text = event.home
            binding.awayTeam.text = event.away
            binding.dateText.text = event.date
            binding.timeText.text = event.time
            binding.executePendingBindings()
        }
    }

}




class EventDiffCallback : DiffUtil.ItemCallback<Event>() {
    override fun areItemsTheSame(oldItem: Event, newItem: Event): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Event, newItem: Event): Boolean {
        return oldItem == newItem
    }
}

