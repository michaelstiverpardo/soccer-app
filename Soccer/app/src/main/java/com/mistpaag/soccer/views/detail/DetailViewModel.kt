package com.mistpaag.soccer.views.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mistpaag.soccer.data.repository.Repository
import com.mistpaag.soccer.dto.Event
import com.mistpaag.soccer.dto.TeamDetail
import com.mistpaag.soccer.dto.responses.OperationResult
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class DetailViewModel(private val repository: Repository) : ViewModel() {
    // TODO: Implement the ViewModel


    val team : LiveData<TeamDetail>
        get()= _team
    private val _team = MutableLiveData<TeamDetail>()

    val nextEvents : LiveData<List<Event>>
        get()= _nextEvents
    private val _nextEvents = MutableLiveData<List<Event>>()

    fun fetchTeam(idTeam: String){
        viewModelScope.launch {
            repository.fetchTeam(idTeam).collect {
                when(it){
                    is OperationResult.Success -> _team.value = it.data
                }
            }
            repository.fetchNextEvents(idTeam).collect {
                when(it){
                    is OperationResult.Success -> _nextEvents.value = it.data
                }

            }
        }
    }
}