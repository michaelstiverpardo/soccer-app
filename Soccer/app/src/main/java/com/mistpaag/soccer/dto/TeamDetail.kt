package com.mistpaag.soccer.dto

import com.google.gson.annotations.SerializedName


data class TeamDetail(
//    @SerializedName("idTeam")
//    val id: String,
    @SerializedName("strTeam")
    val name:String,
    @SerializedName("strDescriptionEN")
    val description:String,
    @SerializedName("intFormedYear")
    val foundationYear:String,
    @SerializedName("strTeamBadge")
    val teamBadge: String,
    @SerializedName("strTeamJersey")
    val teamJersey: String?,
    @SerializedName("strWebsite")
    val webSite:String,
    @SerializedName("strFacebook")
    val facebook:String,
    @SerializedName("strTwitter")
    val twitter:String,
    @SerializedName("strInstagram")
    val instagram:String
){
    fun getDescription(): Description {
       return Description(description, teamJersey)
    }

    fun getSocialNetworks(): List<SocialNetwork> {
        var arrayList: ArrayList<SocialNetwork> = ArrayList()
        arrayList.add(getSocialNetwork(SocialNetwotkType.WEBSITE.type, webSite))
        arrayList.add(getSocialNetwork(SocialNetwotkType.FACEBOOK.type, facebook))
        arrayList.add(getSocialNetwork(SocialNetwotkType.TWITTER.type, twitter))
        arrayList.add(getSocialNetwork(SocialNetwotkType.INSTAGRAM.type, instagram))
        return arrayList
    }

    private fun getSocialNetwork(type:String, urlString: String): SocialNetwork {
        return SocialNetwork(type, getURL(urlString))
    }


    private fun getURL(urlString: String): String {
        return if (! urlString.contains("http")) {
            if (urlString.isNullOrEmpty()) urlString
            else "https://$urlString"
        }else{
            urlString
        }
    }
}
