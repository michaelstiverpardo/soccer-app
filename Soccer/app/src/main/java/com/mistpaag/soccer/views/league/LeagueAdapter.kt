package com.mistpaag.soccer.views.league

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ListAdapter
import androidx.core.net.toUri
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.mistpaag.soccer.R
import com.mistpaag.soccer.databinding.LeagueItemBinding
import com.mistpaag.soccer.dto.Team
import kotlin.properties.Delegates

class LeagueAdapter(val itemClick:(Team)-> Unit) : androidx.recyclerview.widget.ListAdapter<Team, LeagueAdapter.ViewHolder>(LeagueDiffCallback()){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LeagueItemBinding.inflate(LayoutInflater.from(parent.context),parent, false), itemClick)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindTo(getItem(position))
    }

    inner class ViewHolder(private var binding: LeagueItemBinding, var itemClick:(Team)-> Unit) : RecyclerView.ViewHolder(binding.root){
        fun bindTo(team: Team){
            binding.property = team
            binding.cardLayout.setOnClickListener { itemClick(team) }
            binding.executePendingBindings()
        }
    }

}




class LeagueDiffCallback : DiffUtil.ItemCallback<Team>() {
    override fun areItemsTheSame(oldItem: Team, newItem: Team): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Team, newItem: Team): Boolean {
        return oldItem == newItem
    }
}

